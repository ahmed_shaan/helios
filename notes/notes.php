<?php

    // Define a Class

//     class User {
//         //properties (attributes)
//         public $name;
//         //methods (functions)
//         public function sayHello(){
//             return $this->name . ' Says Hello';
//         }
//     }


// //Create new user
// $user1 = new User();
// echo $user1->name="Brad";
// echo $user1->sayHello();

// //Create new user
// $user2 = new User();

// echo $user2->name;
// echo $user2->sayHello();


// echo "<br>";


// //Constructors

// class Users {
//     //properties (attributes)
//     public $name;
//     public $age;

//     //constructor
//     public function __construct($name, $age){
//        //echo "constructor ran";
//        echo __CLASS__ . ' instantiated<br>';
//        $this->name = $name;
//        $this->age = $age;
//     }
//     //methods (functions)
//     public function sayHello(){
//         return $this->name . ' Says Hello';
//     }

//     //destructor
//     public function __destruct(){
//         echo 'destructor ran...';
//     }
// }


// $user1 = new Users('Brad', 26);
// // echo $user1->name;
// // echo $user1->sayHello();





//Access Modifiers, Getters and Setters

// class Userz {
//     private $name;
//     private $age;

//     public function __construct($name, $age){
//         $this->name = $name;
//         $this->age = $age;

//     }

//     public function getName(){
//         return $this->name;
//     }

//     public function setName($name){
//         $this->name = $name;
//     }


//     //__get MAGIC method

//     public function __get($property){
//         if(property_exists($this,$property)){
//             return $this->property;
//         }
//     }

//     //__set MAGIC method
//     public function __set($property, $value){
//         if(property_exists($this,$property)){
//             return $this->property = $value;
//         }
//         return $this;
//     }
// }

// $user1 = new Userz('Jon', 40);

// $user1->__set('age', 21);

// echo $user1->__get('age');




//Inheritance

// class User{
//     protected $name = 'Brad';
//     protected $age;

//     public function __construct($name, $age){
//         $this->name = $name;
//         $this->age = $age;
//     }
// }

// class Customer extends User{
//     private $balance;

//     public function __construct($name, $age, $balance){
//         parent::__construct($name, $age);
//         $this->balance = $balance;
//     }

//     public function pay($amount){
//         return $this->name . ' paid $' . $amount;
//     }

//     public function getBalance(){
//         return $this->balance;
//     }
// }



// $customer1 = new Customer('John', 33, 500);
// //echo $customer1->pay(100);

// echo $customer1->getBalance();




//Static Methods and Properties

class User{
    public $name;
    public $age;
    public static $minPassLength = 6;

    public static function validatePass($pass){
        if(strlen($pass) >= self::$minPassLength){
            return true;
        }else{
            return false;
        }
    }
}


$password = "hello";

if(User::validatePass($password)){
    echo 'Password Valid';
} else {
    echo 'Password NOT Valid';
}






//What is MVC?

- Model View Controller
- Software Design Pattern
- One of the Most frequent used patterns
- Seperates application fucntions 
- Promotes organization

// The Model
- Data Related Logic 
- Intacts with the database (SELECT, INSERT, UPDATE, DELETE)
- Communicates with Controller
- Can sometimes update the view

//The View
 - What user sees in the browser (UI)
 - Ususally consists of HTML and CSS
 - Communicates with controller
 - Can be passed dynamic values from controller

 //The Controller

- Recieves input from the url, form, view, etc
- Processes requests (GET, POST,etc)
- Gets data from the model
- Passes data to the view




//Workflow


app.com/index.php?url=posts/add (we have to load posts controller)
app.com/index.php?url=posts/edit/1

class Posts{
    public function __construct(){
        $this->postModel = $this->model('post');
    }

    public function index(){

    }

    public function add(){

    }

    public function edit($id){
        $post = $this->postModel->fetchPosts($id);

        $this->view('edit', ['post' => $post]);
    }


}


<h1><?php echo $data['title'];?></h1>