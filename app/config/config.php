<?php
//config.php 

// DB Params
define ('DB_HOST', 'localhost');
define ('DB_USER', 'root');
define ('DB_PASS', 'root');
define ('DB_NAME', 'selene');


// App Root
define ('APPROOT', dirname(dirname(__FILE__)));

//URL Root
define('URLROOT', 'http://localhost/helios');

//Site Name
define('SITENAME', 'Helios');

